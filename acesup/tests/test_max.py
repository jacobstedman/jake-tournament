import unittest

from playingcards.basic.card import Card, CardSuit, CardRank, is_same_card
from acesup.brains.max import MaxAcesUpBrain
from acesup.game.game import AcesUpGame

class AcesUpMaxBrainTestCase(unittest.TestCase):

    def test_max_brain_1(self):
        brain = MaxAcesUpBrain()
        g = AcesUpGame(brain, None)
        g.piles[0].append(Card(CardSuit.CLUBS, CardRank.ACE))
        g.piles[1].append(Card(CardSuit.DIAMONDS, CardRank.FIVE))
        g.piles[2].append(Card(CardSuit.SPADES, CardRank.FOUR))
        g.piles[2].append(Card(CardSuit.SPADES, CardRank.KING))    
        g.cards_dealt()

        self.assertEqual(len(g.piles[0]), 1)
        self.assertEqual(len(g.piles[1]), 1)
        self.assertEqual(len(g.piles[2]), 0)
        self.assertEqual(len(g.piles[3]), 1)
        self.assertEqual(g.piles[3][0].suit, CardSuit.SPADES)
        self.assertEqual(g.piles[3][0].rank, CardRank.KING)
        self.assertEqual(len(g.discarded), 1)
        self.assertEqual(g.discarded[0].suit, CardSuit.SPADES)
        self.assertEqual(g.discarded[0].rank, CardRank.FOUR)
       
    def test_max_brain_2(self):
        brain = MaxAcesUpBrain()
        g = AcesUpGame(brain, None)
        g.piles[1].append(Card(CardSuit.CLUBS, CardRank.KING))
        g.piles[2].append(Card(CardSuit.CLUBS, CardRank.TEN))
        g.piles[2].append(Card(CardSuit.SPADES, CardRank.KING))
        g.piles[3].append(Card(CardSuit.HEARTS, CardRank.ACE))   
        g.piles[3].append(Card(CardSuit.DIAMONDS, CardRank.ACE))     
        g.cards_dealt()

        # Max is lazy, so no point in moving ace of diamonds down if it doesn't improve game value
        self.assertEqual(len(g.piles[0]), 1)
        self.assertTrue(is_same_card(g.piles[0][0], Card(CardSuit.SPADES, CardRank.KING)))
        self.assertEqual(len(g.piles[1]), 1)
        self.assertTrue(is_same_card(g.piles[1][0], Card(CardSuit.CLUBS, CardRank.KING)))
        self.assertEqual(len(g.piles[2]), 0)
        self.assertEqual(len(g.piles[3]), 2)
        self.assertTrue(is_same_card(g.piles[3][0], Card(CardSuit.HEARTS, CardRank.ACE)))
        self.assertTrue(is_same_card(g.piles[3][1], Card(CardSuit.DIAMONDS, CardRank.ACE)))
        self.assertEqual(len(g.discarded), 1)
        self.assertTrue(is_same_card(g.discarded[0], Card(CardSuit.CLUBS, CardRank.TEN)))

    def test_max_brain_complex(self):
        brain = MaxAcesUpBrain()
        g = AcesUpGame(brain, None)

        # Deal once
        g.piles[0].append(Card(CardSuit.DIAMONDS, CardRank.NINE))
        g.piles[1].append(Card(CardSuit.CLUBS, CardRank.NINE))
        g.piles[2].append(Card(CardSuit.CLUBS, CardRank.FOUR))
        g.piles[3].append(Card(CardSuit.HEARTS, CardRank.KING))       
        g.cards_dealt()
        self.assertTrue(is_same_card(g.discarded[0], Card(CardSuit.CLUBS, CardRank.FOUR)))

        # Deal twice
        g.piles[0].append(Card(CardSuit.CLUBS, CardRank.TWO))
        g.piles[1].append(Card(CardSuit.DIAMONDS, CardRank.SIX))
        g.piles[2].append(Card(CardSuit.SPADES, CardRank.TEN))
        g.piles[3].append(Card(CardSuit.SPADES, CardRank.FIVE))       
        g.cards_dealt()
        self.assertTrue(is_same_card(g.discarded[-1], Card(CardSuit.SPADES, CardRank.FIVE)))

        # Deal three 
        g.piles[0].append(Card(CardSuit.HEARTS, CardRank.NINE))
        g.piles[1].append(Card(CardSuit.DIAMONDS, CardRank.ACE))
        g.piles[2].append(Card(CardSuit.CLUBS, CardRank.KING))
        g.piles[3].append(Card(CardSuit.DIAMONDS, CardRank.QUEEN))       
        g.cards_dealt()

        # Expect a lot of pruning
        self.assertTrue(is_same_card(g.discarded[2], Card(CardSuit.DIAMONDS, CardRank.QUEEN)))
        self.assertTrue(is_same_card(g.discarded[3], Card(CardSuit.HEARTS, CardRank.NINE)))
        self.assertTrue(is_same_card(g.discarded[4], Card(CardSuit.CLUBS, CardRank.TWO)))
        self.assertTrue(is_same_card(g.discarded[5], Card(CardSuit.DIAMONDS, CardRank.NINE)))
        # Heard we move ACE of DIAMONDS from [1] to [0]
        self.assertTrue(is_same_card(g.discarded[6], Card(CardSuit.DIAMONDS, CardRank.SIX)))
        self.assertTrue(is_same_card(g.discarded[7], Card(CardSuit.CLUBS, CardRank.NINE)))

        # Check expected shape 
        self.assertEqual(len(g.piles[0]), 1)
        self.assertTrue(is_same_card(g.piles[0][0], Card(CardSuit.DIAMONDS, CardRank.ACE)))
        self.assertEqual(len(g.piles[1]), 0)
        self.assertEqual(len(g.piles[2]), 2)
        self.assertTrue(is_same_card(g.piles[2][0], Card(CardSuit.SPADES, CardRank.TEN)))
        self.assertTrue(is_same_card(g.piles[2][1], Card(CardSuit.CLUBS, CardRank.KING)))
        self.assertEqual(len(g.piles[3]), 1)
        self.assertTrue(is_same_card(g.piles[3][0], Card(CardSuit.HEARTS, CardRank.KING)))
     