import unittest

from playingcards.basic.card import Card, CardSuit, CardRank
from acesup.brains.random import RandomAcesUpBrain
from acesup.game.game import AcesUpGame

class AcesUpGameTestCase(unittest.TestCase):

    def test_initial_cards(self):
        g = AcesUpGame(RandomAcesUpBrain(0.0, 0.0), None)
        self.assertEqual(len(g.stock), 52)
        self.assertEqual(len(g.discarded), 0)
        self.assertEqual(len(g.piles), 4)
        self.assertEqual(len(g.piles[0]), 0)

    def test_discard_4_cards(self):
        g = AcesUpGame(RandomAcesUpBrain(0.0, 0.0), None)

        g.piles[0].append(Card(CardSuit.CLUBS, CardRank.THREE))
        g.piles[1].append(Card(CardSuit.CLUBS, CardRank.ACE))
        g.piles[2].append(Card(CardSuit.HEARTS, CardRank.FIVE))
        g.piles[3].append(Card(CardSuit.HEARTS, CardRank.SEVEN))
        g.discard_all_but_the_highest_ranking()
        self.assertEqual(len(g.discarded), 2)
        self.assertEqual(len(g.piles[0]), 0)
        self.assertEqual(len(g.piles[1]), 1)
        self.assertEqual(len(g.piles[2]), 0)
        self.assertEqual(len(g.piles[3]), 1)

        g.piles[0].append(Card(CardSuit.CLUBS, CardRank.FIVE))
        g.piles[1].append(Card(CardSuit.CLUBS, CardRank.FOUR))
        g.piles[2].append(Card(CardSuit.SPADES, CardRank.EIGHT))
        g.piles[3].append(Card(CardSuit.DIAMONDS, CardRank.SEVEN))
        g.discard_all_but_the_highest_ranking()
        self.assertEqual(len(g.discarded), 4)
        self.assertEqual(len(g.piles[0]), 0)
        self.assertEqual(len(g.piles[1]), 1)
        self.assertEqual(len(g.piles[2]), 1)
        self.assertEqual(len(g.piles[3]), 2)
