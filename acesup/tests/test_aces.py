import unittest

from playingcards.basic.card import Card, CardSuit, CardRank, is_same_card
from acesup.brains.aces import AlwaysAcesAcesUpBrain
from acesup.game.game import AcesUpGame

class AlwaysAcesAcesUpBrainTestCase(unittest.TestCase):

    def test_always_aces_brain(self):
        brain = AlwaysAcesAcesUpBrain()
        g = AcesUpGame(brain, None)

        # Deal once
        g.piles[0].append(Card(CardSuit.DIAMONDS, CardRank.ACE))
        g.piles[0].append(Card(CardSuit.HEARTS, CardRank.ACE))
        g.piles[2].append(Card(CardSuit.CLUBS, CardRank.ACE))
        g.piles[3].append(Card(CardSuit.SPADES, CardRank.ACE))       
        g.cards_dealt()
     
        # Expect ace of hearts moved to pile [1]
        self.assertEqual(len(g.piles[0]), 1)
        self.assertTrue(is_same_card(g.piles[0][0], Card(CardSuit.DIAMONDS, CardRank.ACE)))
        self.assertEqual(len(g.piles[1]), 1)
        self.assertTrue(is_same_card(g.piles[1][0], Card(CardSuit.HEARTS, CardRank.ACE)))
        self.assertEqual(len(g.piles[2]), 1)
        self.assertTrue(is_same_card(g.piles[2][0], Card(CardSuit.CLUBS, CardRank.ACE)))
        self.assertEqual(len(g.piles[3]), 1)
        self.assertTrue(is_same_card(g.piles[3][0], Card(CardSuit.SPADES, CardRank.ACE)))
     