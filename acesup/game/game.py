from playingcards.html.pretty_card import PrettyCard
from playingcards.basic.pile import Pile 
from acesup.game.pile import AcesUpPile
from playingcards.basic.deck import Deck 

from acesup.game.primitive import AcesUpGamePrimitive

class AcesUpGame(AcesUpGamePrimitive):

    def __init__(self, brain, output):
        super().__init__(brain)
        self.output = output
        self.stock = Deck(PrettyCard) 
        self.stock.shuffle()
        self.initial_stock = Deck(PrettyCard)
        self.initial_stock.deserialize(self.stock.serialized())
        self.logged_turns = []

    # Rules: https://en.wikipedia.org/wiki/Aces_Up
    def play(self):

        while not self.stock.empty:
            
             # 1. Deal four cards in a row face up.
            self.deal_4_cards()
            
            # Do step 2-4 
            self.cards_dealt()

            # 5. When there are no more cards to move or remove, deal out the next four cards from the deck face-up onto each pile.
            # 6. Repeat Step 2, using only the visible, or top, cards on each of the four piles.

        # 7. When the last four cards have been dealt out and any moves made, the game is over. 
        # The fewer cards left in the tableau, the better. To win is to have only the four aces left.
        self.html_log("No more cards left.")
        if self.won:
            self.html_log(f"We won ({len(self.discarded)} cards discarded)!")

    def deal_4_cards(self):
        assert len(self.stock) % AcesUpGamePrimitive.NUMBER_OF_PILES == 0
        cards_dealt = []
        for i in range(AcesUpGamePrimitive.NUMBER_OF_PILES):
            card = self.stock.pop_first()
            cards_dealt.append(card)
            self.piles[i].append(card)
        self.log_cards_dealt(cards_dealt)

    @property
    def won(self):
        return len(self.discarded) == (52 - AcesUpGamePrimitive.NUMBER_OF_PILES)
    
    def log_cards_dealt(self, cards):
        self.logged_turns.append({
            'event': 'dealt',
            'cards': [card.text_str() for card in cards],
        })
        self.html_print_all_piles(f"Dealt {len(cards)} cards")

    def log_card_discarded(self, pile, card):
        self.logged_turns.append({
            'event': 'discarded',
            'pile': pile,
            'card': card.text_str(),
        })
        self.html_print_all_piles(f"Discarded top card from pile {pile+1}")     # from 0 to 1-indexed
    
    def log_card_moved(self, from_pile, to_pile, card):
        self.logged_turns.append({
            'event': 'moved',
            'from_pile': from_pile,
            'to_pile': to_pile,
            'card': card.text_str(),
        })
        self.html_print_all_piles(f"Moved top card from {from_pile+1} to {to_pile+1}")       # from 0 to 1-indexed

    def html_print_all_piles(self, headline):

        # Rotate matrix
        pile_length = [len(p) for p in self.piles]
        max_pile_length = max(pile_length)
        matrix = [[None for _ in range(AcesUpGamePrimitive.NUMBER_OF_PILES)] for _ in range(max_pile_length)]
        for col, p in enumerate(self.piles):
            for row, card in enumerate(p.list_of_cards):
                matrix[row][col] = card

        # Write HTML
        self.html_log(f"{headline}")
        self.html_log("<table>")
        for row_content in matrix:
            self.html_log("<tr>")
            for cell in row_content:
                if cell:
                    self.html_log(f"<td>{cell}</td>")
                else:
                    self.html_log("<td width=60>&nbsp;</td>")
            self.html_log("</tr>")
        self.html_log("</table><p>")

    def html_log(self, s):
        if self.output is not None:
            self.output.write(s + "\n")
    
    def serialize(self):
        serialized = {
            'in': {
                'brain': str(type(self.brain).__name__),
                'initial_stock': self.initial_stock.serialized(),
            },
            'turns': self.logged_turns,
            'out': super().serialize(),
        }
        return serialized