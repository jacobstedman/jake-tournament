from playingcards.html.pretty_pile import PrettyPile 
from playingcards.basic.card import CardRank

class AcesUpPile(PrettyPile):

    def discard_top_card_if_possible(self, our_index, all_piles):
        if self.empty:
            return None
        number_of_piles = len(all_piles)
        for i in range(number_of_piles):
            if i != our_index and not all_piles[i].empty:
                if all_piles[i].top_card.suit == self.top_card.suit:
                    if self.is_first_higher(all_piles[i].top_card.rank, self.top_card.rank):
                        card_to_discard = self.pop_last()
                        return card_to_discard
        return None
    
    # Don't trust the rank enum on aces, since in the enum they rank low, but in this game they should rank high
    def is_first_higher(self, r1, r2):
        assert r1 != r2, f"Two cards can't have same rank since they are in the same suit, but {r1} == {r2}"
        if r1 == CardRank.ACE:
            return True 
        elif r2 == CardRank.ACE:
            return False 
        else:
            return r1.value > r2.value