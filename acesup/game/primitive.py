from playingcards.html.pretty_card import PrettyCard
from playingcards.basic.pile import Pile 
from acesup.game.pile import AcesUpPile
from playingcards.basic.deck import Deck 

class AcesUpGamePrimitive:

    NUMBER_OF_PILES = 4

    def __init__(self, brain):
        self.brain = brain 
        self.piles = [AcesUpPile(PrettyCard) for _ in range(AcesUpGamePrimitive.NUMBER_OF_PILES)]
        self.discarded = Pile(PrettyCard) 

    # Step 2-4 from https://en.wikipedia.org/wiki/Aces_Up happens here
    def cards_dealt(self):

        empty_spaces = True
        while empty_spaces:

            empty_spaces = False

            # Do step 2 and 3
            self.discard_all_but_the_highest_ranking()
    
            # 4. Whenever there are any empty spaces, you may choose the top card of another pile to be 
            # put into the empty space. After you do this, go to Step 2.
            for to_pile in range(AcesUpGamePrimitive.NUMBER_OF_PILES):
                if self.piles[to_pile].empty:
                    from_pile = self.brain.should_we_move_any_top_card_to_empty_pile(to_pile, self)
                    if from_pile is not None:
                        self.move_card(from_pile, to_pile)
                        empty_spaces = True
                        break       # leave the inner loop, discard all but the highest ranking and try search for more empty spaces

    def discard_all_but_the_highest_ranking(self):
        # 2. If there are two or more cards of the same suit, discard all but the highest-ranked card of that suit. Aces rank high.
        # 3. Repeat step 2 until there are no more pairs of cards with the same suit.  
        look_for_pairs_with_the_same_suit = True
        while look_for_pairs_with_the_same_suit:
            look_for_pairs_with_the_same_suit = False
            for i in range(AcesUpGamePrimitive.NUMBER_OF_PILES):
                card_to_discard = self.piles[i].discard_top_card_if_possible(i, self.piles)
                if card_to_discard:
                    self.log_card_discarded(i, card_to_discard)
                    self.discarded.append(card_to_discard)
                    look_for_pairs_with_the_same_suit = True

    def move_card(self, from_pile, to_pile):
        assert len(self.piles[from_pile]) > 1
        assert len(self.piles[to_pile]) == 0
        card_to_move = self.piles[from_pile].pop_last()
        self.piles[to_pile].append(card_to_move)
        self.log_card_moved(from_pile, to_pile, card_to_move)

    @property
    def value(self):
        return len(self.discarded)

    def log_cards_dealt(self, cards):
        pass

    def log_card_discarded(self, pile, card):
        pass
    
    def log_card_moved(self, from_pile, to_pile, card):
        pass

    def serialize(self):
        serialized = {
            'piles': [pile.serialized() for pile in self.piles],
            'discarded': self.discarded.serialized(),
        }
        return serialized
    
    def deserialize(self, serialized):
        for idx, pile in enumerate(serialized['piles']):
            self.piles[idx].deserialize(pile)
        self.discarded.deserialize(serialized['discarded'])