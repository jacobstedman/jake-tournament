import yaml

from acesup.game.game import AcesUpGame
from acesup.brains.random import RandomAcesUpBrain
from acesup.brains.max import MaxAcesUpBrain
from acesup.brains.aces import AlwaysAcesAcesUpBrain

class AcesUpTournament:

    LOG_MOVES = True

    def __init__(self):
        pass

    def play_tournament(self, iterations, type_of_brain):
        # set up
        self.iterations = iterations
        self.construct_brain(type_of_brain)
        self.wins = 0

        # run game
        for i in range(self.iterations):
            if AcesUpTournament.LOG_MOVES:
                f_html = open(f"acesup/html/round_{i:03}.html", "w")
                with f_html:
                    f_serialized = open(f"acesup/serialized/round_{i:03}.yaml", "w")
                    with f_serialized:
                        self.play_one_round(f"{i:03}", f_html, f_serialized)
            else:
                self.play_one_round(f"{i:03}", None, None)

    def get_stats(self):
        return str(round(100*self.wins/self.iterations,1))+'%' 

    def play_one_round(self, round_id, f_html, f_serialized):
        self.game = AcesUpGame(self.brain, f_html)
        self.game.play()
        if self.game.won:
            self.wins += 1 
        if f_serialized:
            serialized = self.game.serialize()
            yaml.dump(serialized, f_serialized)

    def construct_brain(self, brain_name):
        if brain_name == 'NeverBrain':
            self.brain = RandomAcesUpBrain(1.0, 0)
        elif brain_name == 'RandomBrain':
            self.brain = RandomAcesUpBrain(0.5, 0)
        elif brain_name == 'AlwaysBrain':
            self.brain = RandomAcesUpBrain(0.0, 0)
        elif brain_name == 'MaxBrain':
            self.brain = MaxAcesUpBrain()
        elif brain_name == 'AlwaysAcesBrain':
            self.brain = AlwaysAcesAcesUpBrain()
        else:
            raise Exception(f"'{brain_name}' not currently supported.")
