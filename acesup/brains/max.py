from acesup.brains.abstract import AbstractAcesUpBrain
from acesup.game.primitive import AcesUpGamePrimitive

class MaxAcesUpBrain(AbstractAcesUpBrain):

    def should_we_move_any_top_card_to_empty_pile(self, to_pile, game):
        pile_values = []
        for from_pile in range(len(game.piles)):
            if from_pile == to_pile:
                # make sure we never pick this, since it's an illegal move (discarded is always positive)
                value_of_this_pile = -1
            elif len(game.piles[from_pile]) <= 1:      
                # not point moving from piles with 1 card, so value of that game is value of doing nothing
                value_of_this_pile = -1
            else:
                game_if_we_move_this_pile = AcesUpGamePrimitive(MaxAcesUpBrain())
                game_if_we_move_this_pile.deserialize(game.serialize()['out'])
                #print("before", game_if_we_move_this_pile.serialize())
                game_if_we_move_this_pile.move_card(from_pile, to_pile)
                game_if_we_move_this_pile.discard_all_but_the_highest_ranking()
                #print("after", game_if_we_move_this_pile.serialize())
                value_of_this_pile = game_if_we_move_this_pile.value
            pile_values.append(value_of_this_pile)
        #print(pile_values)
        # be lazy: if we cannot improve on today's game value, do nothing
        if max(pile_values) == -1 or max(pile_values) == game.value:
            return None
        else:
            most_promising_from_pile = pile_values.index(max(pile_values))
            return most_promising_from_pile