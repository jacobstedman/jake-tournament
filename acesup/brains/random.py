# Brain parameters
# * Courage: tendency to not wait to fill empty spaces and see if something better comes up
# * Increasing courage: share that the courage decreases every time a card is dealt

import random

from acesup.brains.abstract import AbstractAcesUpBrain

class RandomAcesUpBrain(AbstractAcesUpBrain):

    def __init__(self, courage, courage_increase):
        self.courage = courage 
        self.courage_increase = courage_increase

    def should_we_move_any_top_card_to_empty_pile(self, to_pile, game):
        #print(f"Should we move something to pile {to_pile}?")
        roll_of_dice = random.random()
        #print(roll_of_dice, self.courage)
        courage = self.courage - self.courage_increase * self.cards_played(game)
        if roll_of_dice > courage:
            for from_pile in range(len(game.piles)):
                if from_pile != to_pile and len(game.piles[from_pile]) > 1:      # not point moving from piles with 1 card
                    return from_pile
        return None
                
    def cards_played(self, game):
        played = len(game.discarded)
        for p in game.piles:
            played += len(p)
        return played
        
