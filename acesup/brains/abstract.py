import abc

class AbstractAcesUpBrain:

    @abc.abstractmethod
    def should_we_move_any_top_card_to_empty_pile(self, to_pile, game):
        pass