from playingcards.basic.card import CardRank
from acesup.brains.abstract import AbstractAcesUpBrain
from acesup.game.primitive import AcesUpGamePrimitive

class AlwaysAcesAcesUpBrain(AbstractAcesUpBrain):

    def should_we_move_any_top_card_to_empty_pile(self, to_pile, game):
        pile_values = []
        for from_pile in range(len(game.piles)):
            if from_pile == to_pile or len(game.piles[from_pile]) <= 1:      
                # don't do illegal moves or moves piles with only 1 card
                return None
            elif game.piles[from_pile][-1].rank == CardRank.ACE:
                # always move if we can move an ace
                return from_pile 
            else:
                # otherwise never move
                return None
               