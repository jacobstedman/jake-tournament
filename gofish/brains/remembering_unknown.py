from gofish.brains.remembering import RememberingBrain

class RememberingUnknownBrain(RememberingBrain):

    def reset(self):
        super().reset()
        self.others_cards = [0] * self.no_of_players

    def inform_about_new_change_in_card_count(self, player_id, no_of_cards):
        self.others_cards[player_id] += no_of_cards

    def get_unknown_count(self, player_id):
        count = self.others_cards[player_id]
        for rank in self.possessions[player_id].keys():
            count -= self.possessions[player_id][rank]
        return count

    def get_player_with_highest_unknown_count(self):
        # ok if we only catch one if several with the same count
        # if there are no players with any count, just pick any (which is not us)
        max_count = -1
        max_player = 0 if self.id != 0 else 1
        for player_id in range(self.no_of_players):
            if player_id != self.id:
                this_count = self.get_unknown_count(player_id)
                if this_count > max_count:
                    max_count = this_count
                    max_player = player_id
        return max_player

    def player_to_ask(self):
        # if we have synergies (we have cards someone else has, trust RememberingBrain). if not, ask the players with most unknown.
        if self.synergies.any:
            return super().player_to_ask()
        else:
            return self.get_player_with_highest_unknown_count()
