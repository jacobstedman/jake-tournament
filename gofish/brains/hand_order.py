from gofish.brains.random import RandomBrain
from gofish.game.ask import Ask

class HandOrderBrain(RandomBrain):

    def rank_to_ask_for(self):
        # Always try to ask to ask for the lowest position (we think of this has lowest knowledge about whether the others has it)
        return self.hand[0].rank

    def ask_executed(self, ask):
        # Once we ask is executed, move the rank we asked about to the highest position
        # (since then we know that at least one other player no [longer] has it)
        idx, position = self.hand.find_position_by_rank(ask.rank)
        del self.hand[idx]
        self.hand.append(position)

    def we_got_new_card_from_pool(self, card):
        # find the card we just inserted and move it the front (lowest number) -- performs better if we do this also for ranks we had before
        idx, position = self.hand.find_position_by_rank(card.rank)
        del self.hand[idx]
        self.hand.insert(0, position)
