import random

from gofish.brains.abc import AbstractBrain

class RandomBrain(AbstractBrain):

    def player_to_ask(self):
        # Come up with a random player to ask which is not ourselves
        possible_player_ids = [i for i in range(0, self.no_of_players)]
        del possible_player_ids[self.id]
        recipent_id = possible_player_ids[random.randint(0, len(possible_player_ids)-1)]
        return recipent_id

    def rank_to_ask_for(self):
        # Come up with a random position to ask about
        position = random.randint(0, self.hand.no_of_positions-1)
        return self.hand[position].rank

    def ask_executed(self, ask):
        pass

    def inform_about_new_change_in_card_count(self, player_id, no_of_cards):
        pass

    def inform_about_new_possession(self, rank, player_id, no_of_cards):
        pass

    def inform_about_lack_of_possession(self, rank, player_id):
        pass

    def we_got_new_card_from_pool(self, card):
        pass

    def get_possessions(self):
        return ''
