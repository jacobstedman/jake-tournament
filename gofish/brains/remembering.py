from playingcards.basic.card import CardRank
from gofish.brains.hand_order import HandOrderBrain
from gofish.game.ask import Ask

class RanksPossessedByBothUsAndOthers():

    def __init__(self):
        self.synergies = {}

    def note_synergy(self, rank, player_id):
        assert isinstance(rank, CardRank), f"Expected CardRank, got {type(rank)}"
        assert isinstance(player_id, int), f"Expected int, got {type(rank)}"
        if rank not in self.synergies:
            self.synergies[rank] = []
        self.synergies[rank].append(player_id)

    def get_first_rank(self):
        first_key = list(self.synergies.keys())[0]
        return first_key

    def get_first_player_for_rank(self, rank):
        return self.synergies[rank][0]

    @property
    def any(self):
        return len(self.synergies.keys()) > 0

class RememberingBrain(HandOrderBrain):

    def reset(self):
        self.possessions = [{} for i in range(self.no_of_players)]

    def generate_ask(self):
        self.update_synergies()
        return super().generate_ask()

    def update_synergies(self):
        self.synergies = RanksPossessedByBothUsAndOthers()
        for hand_position in self.hand:
            for player_id, player_possessions in enumerate(self.possessions):
                # don't look for our own cards (duh!), but ensure this other player possesses something we also have (otherwise we can't ask)
                if player_id != self.id and hand_position.rank in player_possessions:
                    self.synergies.note_synergy(hand_position.rank, player_id)

    def player_to_ask(self):
        if self.synergies.any:
            return self.synergies.get_first_player_for_rank(self.synergies.get_first_rank())
        else:
            return super().player_to_ask()

    def rank_to_ask_for(self):
        if self.synergies.any:
            return self.synergies.get_first_rank()
        else:
            return super().rank_to_ask_for()

    def inform_about_new_possession(self, rank, player_id, no_of_cards):
        #print(rank, player_id, no_of_cards)
        if not rank in self.possessions[player_id]:
            self.possessions[player_id][rank] = 0
        self.possessions[player_id][rank] += no_of_cards
        # we cannot possess fewer than 1. If we get a posession signal (A asks B, is rejected), it still A has 1, even if we didn't know it before,
        # and she didn't get more
        if self.possessions[player_id][rank] == 0:
            self.possessions[player_id][rank] = 1

    def inform_about_lack_of_possession(self, rank, player_id):
        if rank in self.possessions[player_id]:     # We might not have known about it, nothing to forget then
            del self.possessions[player_id][rank]

    def get_possessions(self):
        possession_list = []
        for idx, p in enumerate(self.possessions):
            if idx != self.id and len(p) > 0:
                possession_list.append(f"Player {idx} has {', '.join([r.name + 's' for r in p.keys()])}")
        if len(possession_list) > 0:
            return 'She knows that ' + ' and '.join(possession_list) + '.'
        else:
            return ''
