import abc

from gofish.game.ask import Ask

class AbstractBrain:

    def __init__(self, no_of_players):
        self.no_of_players = no_of_players
        self.reset()

    def set_id(self, id):
        self.id = id

    def set_hand(self, hand):
        self.hand = hand

    def generate_ask(self):
        player_to_ask = self.player_to_ask()
        rank_to_ask_for = self.rank_to_ask_for()
        return Ask(player_to_ask, rank_to_ask_for)

    def reset(self):
        pass

    @abc.abstractmethod
    def player_to_ask(self):
        pass

    @abc.abstractmethod
    def rank_to_ask_for(self):
        pass

    @abc.abstractmethod
    def ask_executed(self, ask):
        pass

    @abc.abstractmethod
    def inform_about_new_change_in_card_count(self, player_id, no_of_cards):
        pass

    @abc.abstractmethod
    def inform_about_new_possession(self, rank, player_id, no_of_cards):
        pass

    @abc.abstractmethod
    def inform_about_lack_of_possession(self, rank, player_id):
        pass

    @abc.abstractmethod
    def we_got_new_card_from_pool(self, card):
        pass

    @abc.abstractmethod
    def get_possessions(self):
        pass
