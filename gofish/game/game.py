from playingcards.html.pretty_deck import PrettyDeck
from playingcards.html.pretty_card import PrettyCard
from playingcards.basic.card import CardRank
from gofish.game.player import GoFishPlayer
from gofish.game.ask import Ask
from gofish.game.broadcaster import Broadcaster

class GoFishGame():

    def __init__(self, brains, output):
        self.players = []
        self.output = output
        for id, brain in enumerate(brains):
            brain.reset()
            self.players.append(GoFishPlayer(id, brain, output))
        self.logged_turns = []
        self.broadcaster = Broadcaster(self.players, self.output)

    def init_pool(self):
        self.pool = PrettyDeck(PrettyCard)
        self.pool.shuffle()
        self.initial_deck_state = self.pool.serialized()

    def load_pool(self, serialized_pool):
        self.pool = PrettyDeck(PrettyCard)
        self.pool.deserialize(serialized_pool)
        self.initial_deck_state = None

    def deal_initial_cards(self):
        no_of_initial_cards = 7 if len(self.players) <= 3 else 5
        for player in self.players:
            self.broadcaster.new_turn(player.id)
            for i in range(no_of_initial_cards):
                card_from_pool = self.pool.pop_first()
                player.accept_card(card_from_pool)
                self.broadcaster.picked_up_card_from_pool(card_from_pool, public=False, log=False)
            player.move_complete_positions_to_books_if_any()
            # theoretical, we could get all cards for one rank initially
            # in that case, no need to broadcast to other players

    def __str__(self):
        return '<p>' + '    '.join([str(p) for p in self.players]) + '</p>\n'

    def play(self):
        current_player_id = 0    # Start with player 0
        while self.books < len(CardRank):
            successful_move = self.play_one_turn(current_player_id, None, None)
            if not successful_move:
                current_player_id = self.next_player(current_player_id)
        self.broadcaster.log(str(self))

    def load_turns(self, turns):
        for turn in turns:
            (from_player, to_player, rank) = turn.split(',')
            if from_player.isdigit() and to_player.isdigit() and isinstance(rank, str):
                self.play_one_turn(int(to_player), int(from_player), CardRank[rank])
            else:
                raise Exception(f"Invalid turn loaded ('{turn}')")
        self.broadcaster.log(str(self))

    def announce_winners(self, round_id):
        winners = self.players_with_most_books()
        winning_no_of_books = len(winners[0].books)
        if len(winners) == 1:
            player = winners[0].id
            details = ', '.join([book.name for book in winners[0].books])
            print(f"{round_id}: Player {player} wins with {winning_no_of_books} books: {details}!")
        else:
            players = ', '.join([str(w.id) for w in winners])
            details =  ' and '.join([ ', '.join([book.name for book in winner.books]) for winner in winners ]) + ", respectively"
            print(f"{round_id}: Players {players} win with {winning_no_of_books} books: {details}!")

    def play_one_turn(self, current_player_id, forced_recipient, forced_rank):

        self.broadcaster.new_turn(current_player_id)

        # If we have no cards hand and pool is empty, give up and let another player try, who might be sitting with a hand
        # Otherwise, pick one up and continue
        if self.players[current_player_id].empty_hand:
            if len(self.pool) == 0:
                # don't log here, that would create a mismatch between the played and the loaded html versions
                return False
            else:
                self.broadcaster.log(str(self))
                card_from_pool = self.pool.pop_first()
                self.players[current_player_id].accept_card_from_pool(card_from_pool)
                self.broadcaster.picked_up_card_from_pool(card_from_pool, public=False)
        else:
            self.broadcaster.log(str(self))

        # Player A asks Player B for cards
        if forced_recipient is None or forced_rank is None:
            ask = self.players[current_player_id].generate_ask()
        else:
            ask = Ask(forced_recipient, forced_rank)
        assert current_player_id != ask.recipient_id
        self.log_turn(ask.recipient_id, current_player_id, ask.rank)
        self.players[current_player_id].ask_executed(ask)
        cards_in_response = self.players[ask.recipient_id].reveal_rank(ask.rank)
        self.broadcaster.asked_for_cards(ask, cards_in_response)

        # If Player B had the cards, transfer them
        if len(cards_in_response) > 0:
            self.players[current_player_id].accept_cards(cards_in_response)
            self.players[ask.recipient_id].remove_from_hand(ask.rank)
            successful_move = True

        # If not, allow Player A to pick from pool (unless it's empty)
        else:
            if len(self.pool) == 0:
                successful_move = False
            else:
                card_from_pool = self.pool.pop_first()
                self.players[current_player_id].accept_card_from_pool(card_from_pool)
                successful_move = card_from_pool.rank == ask.rank
                self.broadcaster.picked_up_card_from_pool(card_from_pool, public=successful_move)

        # If player A now completed all suits for one rank, it forms a book
        completed_ranks = self.players[current_player_id].move_complete_positions_to_books_if_any()
        for rank in completed_ranks:
            self.broadcaster.completed_book(rank)

        return successful_move

    def next_player(self, current_player_id):
        return current_player_id+1 if current_player_id < len(self.players)-1 else 0

    def players_with_most_books(self):
        winners = []
        max_count = -1
        for p in self.players:
            no_of_books_for_this_player = len(p.books)
            if no_of_books_for_this_player > max_count:
                winners = [p]
                max_count = no_of_books_for_this_player
            elif no_of_books_for_this_player == max_count:
                winners.append(p)
                # edge case: there could be two winners with the same amount of books
        return winners

    def log_turn(self, from_player, to_player, rank):
        self.logged_turns.append({
            'from': from_player,
            'to': to_player,
            'rank': rank.name,
        })

    def get_all_books(self):
        books = []
        for p in self.players:
            books.append(p.serialize())
        return books

    def serialize(self):
        serialized = {
            'in': {
                'players': [ str(type(p.brain).__name__) for p in self.players ],
                'initial_deck': self.initial_deck_state,
                'turns': [],
            },
            'out': {
                'books': self.get_all_books(),
            }
        }
        for turn in self.logged_turns:
            serialized['in']['turns'].append(f"{turn['from']},{turn['to']},{turn['rank']}")
        return serialized

    @property
    def books(self):
        count = 0
        for p in self.players:
            count += len(p.books)
        return count
