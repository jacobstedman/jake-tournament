import yaml

from gofish.game.game import GoFishGame
from gofish.brains.random import RandomBrain
from gofish.brains.hand_order import HandOrderBrain
from gofish.brains.remembering import RememberingBrain
from gofish.brains.remembering_unknown import RememberingUnknownBrain

class GoFishTournament:

    LOG_MOVES = True

    def __init__(self):
        pass

    def play_tournament(self, iterations, type_of_brains):
        # set up
        self.iterations = iterations
        self.construct_brains(type_of_brains)
        self.acc_winners = [0] * len(type_of_brains)

        # run game
        for i in range(self.iterations):
            if GoFishTournament.LOG_MOVES:
                f_html = open(f"gofish/html/round_{i:03}.html", "w")
                with f_html:
                    f_serialized = open(f"gofish/serialized/round_{i:03}.yaml", "w")
                    with f_serialized:
                        self.play_one_round(f"{i:03}", f_html, f_serialized)
            else:
                self.one_game(None, None)

    def get_winners(self):
        return ' / '.join([str(round(100*w/self.iterations,1))+'%' for w in self.acc_winners])

    def update_winning_stats(self):
        winners = self.game.players_with_most_books()
        for w in winners:
            self.acc_winners[w.id] += (1/len(winners))

    def play_one_round(self, round_id, f_html, f_serialized):
        self.game = GoFishGame(self.brains, f_html)
        self.game.init_pool()
        self.game.deal_initial_cards()
        self.game.play()
        if f_html:
            self.game.announce_winners(round_id)
        self.update_winning_stats()
        if f_serialized:
            serialized = self.game.serialize()
            yaml.dump(serialized, f_serialized)

    def recreate_selective_games(self, rounds):
        for round in rounds:
            round_id = f"{int(round):03}"
            with open(f"gofish/serialized/round_{round_id}.yaml", "r") as f_serialized:
                serialized = yaml.load(f_serialized, Loader=yaml.FullLoader)
                if GoFishTournament.LOG_MOVES:
                    with open(f"gofish/html/round_{round_id}_recreated.html", "w") as f_html:
                        game = self.recreate_one_round(round_id, f_html, serialized)
                else:
                    game = self.recreate_one_round(round_id, None, serialized)
                self.game.announce_winners(round_id)

    def recreate_one_round(self, round_id, f_html, serialized):
        serialized_brains = serialized["in"]["players"]
        self.construct_brains(serialized_brains)
        self.acc_winners = [0] * len(serialized_brains)
        self.game = GoFishGame(self.brains, f_html)
        self.game.load_pool(serialized["in"]["initial_deck"])
        self.game.deal_initial_cards()
        self.game.load_turns(serialized["in"]["turns"])
        return self.game

    def construct_brains(self, brain_names):
        number_of_players = len(brain_names)
        self.brains = []
        for name in brain_names:
            if name == 'RandomBrain':
                self.brains.append(RandomBrain(number_of_players))
            elif name == 'HandOrderBrain':
                self.brains.append(HandOrderBrain(number_of_players))
            elif name == 'RememberingBrain':
                self.brains.append(RememberingBrain(number_of_players))
            elif name == 'RememberingUnknownBrain':
                self.brains.append(RememberingUnknownBrain(number_of_players))
            else:
                raise Exception(f"Only brain types 'RandomBrain', 'HandOrderBrain', 'RememberingBrain' and 'RememberingUnknownBrain' currently supported (found '{name}')")
