class Ask:

    def __init__(self, recipient_id, rank):
        self.recipient_id = recipient_id
        self.rank = rank

    def __str__(self):
        return f"Ask player {self.recipient_id} for {self.rank.name.capitalize()}"
