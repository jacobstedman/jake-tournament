class Broadcaster:

    def __init__(self, players, output):
        self.players = players
        self.output = output

    def new_turn(self, current_player_id):
        self.current_player_id = current_player_id

    def asked_for_cards(self, ask, cards_in_response):
        no_of_cards = len(cards_in_response)
        if no_of_cards == 0:
            self.log(f"<p>Player {self.current_player_id} asked Player {ask.recipient_id} for all {ask.rank.name}s, was told to go fishing.</p>")
        else:
            self.log(f"<p>Player {self.current_player_id} asked Player {ask.recipient_id} for all {ask.rank.name}s, got {len(cards_in_response)}.</p>")

        # inform everyone that Player B certainly doesn't have the cards anymore (no matter if she did before), and that Player A might have
        # (but even if Player B didn't have the card, it's good to know that Player A asked for it and thus has at least one)
        for idx, p in enumerate(self.players):
            if idx != self.current_player_id:
                p.inform_about_new_possession(ask.rank, self.current_player_id, no_of_cards)
                if no_of_cards != 0:
                    p.inform_about_new_change_in_card_count(self.current_player_id, no_of_cards)
            if idx != ask.recipient_id:
                p.inform_about_lack_of_possession(ask.rank, ask.recipient_id)
                if no_of_cards != 0:
                    p.inform_about_new_change_in_card_count(ask.recipient_id, -no_of_cards)

    def picked_up_card_from_pool(self, card_from_pool, public, log=True):
        if log:
            self.log(f"Player {self.current_player_id} picked up {card_from_pool} from the pool.<br>")
        if public:
            for p in self.all_other_players:
                p.inform_about_new_possession(card_from_pool.rank, self.current_player_id, 1)
        else:
            for p in self.all_other_players:
                p.inform_about_new_change_in_card_count(self.current_player_id, 1)

    def completed_book(self, rank):
        self.log(f"Player {self.current_player_id} got 4 {rank.name.upper()}s. They have been laid down as a book.<br>")
        for p in self.all_other_players:
            p.inform_about_lack_of_possession(rank, self.current_player_id)
            p.inform_about_new_change_in_card_count(self.current_player_id, -4)

    def log(self, s):
        if self.output is not None:
            self.output.write(s + "\n")

    @property
    def all_other_players(self):
        return [p for p in self.players if p.id != self.current_player_id]
