from playingcards.html.pretty_player import PrettyPlayer
from gofish.game.hand import GoFishHand

class GoFishPlayer(PrettyPlayer):

    def __init__(self, player_id, brain, output):
        super().__init__(player_id, brain, output)
        self.hand = GoFishHand()
        self.books = []
        brain.set_id(player_id)
        brain.set_hand(self.hand)

    def move_complete_positions_to_books_if_any(self):
        complete_ranks = self.hand.pop_complete_positions()
        if len(complete_ranks) > 0:
            self.books.extend(complete_ranks)
        return complete_ranks

    def accept_card_from_pool(self, card):
        super().accept_card(card)
        self.brain.we_got_new_card_from_pool(card)

    def serialize(self):
        return [ book.name for book in self.books ]
