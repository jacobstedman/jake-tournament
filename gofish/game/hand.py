from playingcards.html.pretty_hand import PrettyHand

class GoFishHand(PrettyHand):
    
    def pop_complete_positions(self):
        idx_to_pop = []
        ranks_to_pop = []
        for idx, p in enumerate(self.positions):
            if p.complete:
                ranks_to_pop.append(p.rank)
                idx_to_pop.append(idx)      # we don't want to remove it from the list we're iterating through
        for i in idx_to_pop:
            del self.positions[i]
        return ranks_to_pop
