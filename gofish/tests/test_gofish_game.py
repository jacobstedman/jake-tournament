import unittest

from gofish.brains.abc import AbstractBrain
from gofish.game.game import GoFishGame

class GoFishGameTestCase(unittest.TestCase):

    def test_initial_cards(self):
        g = GoFishGame([AbstractBrain(3), AbstractBrain(3), AbstractBrain(3)], None)
        g.init_pool()
        g.deal_initial_cards()
        for player in g.players:
            self.assertEqual(player.hand.no_of_cards, 7)
        self.assertEqual(len(g.pool), 52-3*7)
