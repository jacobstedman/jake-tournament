import unittest

from gofish.brains.remembering import RememberingBrain

class RememberingBrainTestCase(unittest.TestCase):

    def test_remembering_basics(self):
        b = RememberingBrain(2)
        self.assertEqual(b.no_of_players, 2)
