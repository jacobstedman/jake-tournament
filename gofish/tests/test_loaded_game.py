import unittest
import yaml

from gofish.game.tournament import GoFishTournament

class GoFishLoadedGameTestCase(unittest.TestCase):

    def test_loaded_game(self):
        tournament = GoFishTournament()
        with open(f"gofish/tests/round_000.yaml", "r") as f_serialized:
            serialized = yaml.load(f_serialized, Loader=yaml.FullLoader)
            game = tournament.recreate_one_round("000", None, serialized)
            self.assertEqual(game.get_all_books(), serialized['out']['books'])
