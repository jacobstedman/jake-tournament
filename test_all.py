import sys
import unittest

from playingcards.tests.test_card import CardTestCase
from playingcards.tests.test_deck import DeckTestCase
from playingcards.tests.test_hand import HandTestCase
from playingcards.tests.test_player import PlayerTestCase
from playingcards.tests.test_pretty_card import PrettyCardTestCase

from gofish.tests.test_gofish_game import GoFishGameTestCase
from gofish.tests.test_loaded_game import GoFishLoadedGameTestCase
from gofish.tests.test_remembering_brain import RememberingBrainTestCase

from acesup.tests.test_acesup_game import AcesUpGameTestCase
from acesup.tests.test_max import AcesUpMaxBrainTestCase
from acesup.tests.test_aces import AlwaysAcesAcesUpBrainTestCase

# assumes all *TestCase in our local namespace are unittest.TestCase descendents that can be loaded
ALL_TEST_NAMES = [ name for name in locals() if name.endswith('TestCase') ]

def str_2_class(class_name):
    return getattr(sys.modules['test_all'], class_name)

def run_unit_tests():
    all_test_loaders = [ unittest.TestLoader().loadTestsFromTestCase(str_2_class(test_name)) for test_name in ALL_TEST_NAMES ]
    all_tests = unittest.TestSuite(all_test_loaders)
    results = unittest.TextTestRunner(verbosity=2).run(all_tests)
