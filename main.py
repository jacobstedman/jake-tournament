import sys
from datetime import datetime

from test_all import run_unit_tests
from gofish.game.tournament import GoFishTournament
from acesup.game.tournament import AcesUpTournament

start_timestamp = datetime.now()

run_unit_tests()

SYNTAX = "Syntax: python3 main.py play [game] [iterations] [players...] or python3 main.py load [game] [round]\n\n" + \
    "Currently only the games of 'gofish' and 'acesup' are supported for 'play', and only 'gofish' for 'load'."
ARG_VERB = 1
ARG_GAME = 2
ARG_COUNT = 3

def fail(s):
    if s:
        sys.exit(f"{SYNTAX}\n\n{s}")
    else:
        sys.exit(SYNTAX)

def gofish(argv):
    tournament = GoFishTournament()
    if sys.argv[ARG_VERB] == 'play':
        if sys.argv[ARG_COUNT].isdigit():
            if len(sys.argv) >= 6:
                tournament.play_tournament(int(sys.argv[ARG_COUNT]), sys.argv[ARG_COUNT+1:])
                print(f"[{get_time_prefix(start_timestamp)}] {tournament.get_winners()}")
            else:
                fail("Gofish: At least two players needed.")
        else:
            fail("Gofish: Iterations needs to be a number.")
    elif sys.argv[ARG_VERB] == 'load':
        tournament.recreate_selective_games(sys.argv[ARG_COUNT:])
    else:
        fail("Gofish: only 'play' and 'load' currently supported.")

def acesup(argv):
    series = AcesUpTournament()
    if sys.argv[ARG_VERB] == 'play':
        if sys.argv[ARG_COUNT].isdigit():
            if len(sys.argv) == 5:
                series.play_tournament(int(sys.argv[ARG_COUNT]), sys.argv[ARG_COUNT+1])
                print(f"[{get_time_prefix(start_timestamp)}] {series.get_stats()}")
            else:
                fail("AcesUp: Only one player supported.")
        else:
            fail("AcesUp: Iterations needs to be a number.")
    else:
        fail("AcesUp: only 'play' currently supported for 'acesup'")

def get_time_prefix(start_timestamp):
    diff_string = str(datetime.now() - start_timestamp)
    without_hours_and_only_2_decimals = diff_string[2:10]
    return f"{without_hours_and_only_2_decimals}"

if len(sys.argv) < 4:
    fail("Too few arguments.")
elif sys.argv[ARG_GAME] == 'gofish':
    gofish(sys.argv)
elif sys.argv[ARG_GAME] == 'acesup':
    acesup(sys.argv)
else:
    fail(f"Unknown game '{sys.argv[ARG_GAME]}'")
