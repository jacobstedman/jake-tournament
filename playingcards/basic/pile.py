import random

from playingcards.basic.card import Card

class Pile:

    def __init__(self, card_class):
        self.card_class = card_class
        self.list_of_cards = []

    def __len__(self):
        return len(self.list_of_cards)
    
    def __getitem__(self, item):
        return self.list_of_cards[item]

    def __str__(self):
        return str([str(c) for c in self.list_of_cards])

    def shuffle(self):
        if len(self) > 1:
            for i in range(100):
                source_pos = random.randint(0, len(self)-1)
                dest_pos = random.randint(0, len(self)-1)
                temp = self.list_of_cards[dest_pos]
                self.list_of_cards[dest_pos] = self.list_of_cards[source_pos]
                self.list_of_cards[source_pos] = temp

    def pop_first(self):
        return self.list_of_cards.pop(0)
    
    def pop_last(self):
        return self.list_of_cards.pop()
    
    def append(self, i):
        return self.list_of_cards.append(i)
    
    def copy_from(self, other):
        # deep copy of other object
        self.card_class = other.card_class
        self.deserialize(other.serialized())
    
    @property 
    def empty(self):
        return len(self.list_of_cards) == 0
    
    @property 
    def top_card(self):
        assert not self.empty
        return self.list_of_cards[-1]

    def serialized(self):
        return [ card.serialize() for card in self.list_of_cards ]

    def deserialize(self, serialized):
        self.list_of_cards = [ self.card_class.deserialize(card) for card in serialized ]
