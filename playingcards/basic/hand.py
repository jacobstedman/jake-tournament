from playingcards.basic.card import Card, CardRank, CardSuit

class HandPosition():

    def __init__(self, initial_card):
        assert isinstance(initial_card, Card)
        self.rank = initial_card.rank
        self.cards = []
        self.append(initial_card)

    def __len__(self):
        return len(self.cards)

    def __str__(self):
        return f"{self.rank.name.capitalize()} of {' and '.join([c.suit.name.capitalize() for c in self.cards])}"

    def append(self, card):
        assert isinstance(card, Card)
        self.cards.append(card)

    @property
    def complete(self):
        return len(self.cards) == 4

class Hand():

    def __init__(self):
        self.positions = []

    def add(self, card):
        assert isinstance(card, Card)
        idx, matching_position = self.find_position_by_rank(card.rank)
        if matching_position is not None:
            matching_position.append(card)
        else:
            self.positions.append(HandPosition(card))

    def remove(self, rank):
        assert isinstance(rank, CardRank)
        idx, matching_position = self.find_position_by_rank(rank)
        if idx is not None:
            del self.positions[idx]

    def find_position_by_rank(self, this_rank):
        assert isinstance(this_rank, CardRank), f"Expected CardRank, got {type(this_rank)}"
        for idx, position in enumerate(self.positions):
            if position.rank == this_rank:
                return idx, position
        return None, None

    def __contains__(self, this_rank):
        idx, matching_position = self.find_position_by_rank(this_rank)
        return idx is not None

    def __getitem__(self, position):
        return self.positions[position]

    def __setitem__(self, position, value):
        self.positions[position] = value

    def __delitem__(self, position):
        del self.positions[position]

    def append(self, value):
        self.positions.append(value)

    def insert(self, i, value):
        self.positions.insert(i, value)

    def __str__(self):
        if len(self.positions) == 0:
            return "Empty hand"
        else:
            prefix = '1 position' if len(self.positions) == 1 else str(len(self.positions)) + ' positions'
            return prefix + ': ' + ', '.join([str(p) for p in self.positions])

    @property
    def no_of_positions(self):
        return len(self.positions)

    @property
    def no_of_cards(self):
        c = 0
        for position in self.positions:
            c += len(position)
        return c
