from playingcards.basic.hand import Hand
from playingcards.basic.card import Card, CardSuit, CardRank

class Player():

    def __init__(self, player_id, brain, output):
        self.id = player_id
        self.brain = brain
        self.hand = Hand()
        self.output = output

    def __str__(self):
        return f'Player {self.id}: {str(self.hand)}'

    # HAND MANAGEMENT

    @property
    def empty_hand(self):
        return self.hand.no_of_cards == 0

    def reveal_rank(self, rank):
        idx, position = self.hand.find_position_by_rank(rank)
        if idx is None:
            return []
        else:
            return position.cards

    def accept_card(self, card):
        self.hand.add(card)

    def accept_cards(self, cards):
        for card in cards:
            self.accept_card(card)

    def remove_from_hand(self, rank):
        self.hand.remove(rank)

    # BRAIN MANAGEMENT

    def generate_ask(self):
        return self.brain.generate_ask()

    def ask_executed(self, ask):
        return self.brain.ask_executed(ask)

    def inform_about_new_change_in_card_count(self, player_id, no_of_cards):
        self.brain.inform_about_new_change_in_card_count(player_id, no_of_cards)

    def inform_about_new_possession(self, rank, player_id, no_of_cards):
        self.brain.inform_about_new_possession(rank, player_id, no_of_cards)
        current_intel = self.brain.get_possessions()
        if self.output is not None:
            if no_of_cards == 0:
                self.output.write(f"<i>Informed player {self.id} that player {player_id} possesses at least one {rank.name.upper()}. {current_intel}</i><br>\n")
            else:
                self.output.write(f"<i>Informed player {self.id} that player {player_id} now possesses {no_of_cards} more {rank.name.upper()}. {current_intel}</i><br>\n")

    def inform_about_lack_of_possession(self, rank, player_id):
        self.brain.inform_about_lack_of_possession(rank, player_id)
        current_intel = self.brain.get_possessions()
        if self.output is not None:
            self.output.write(f"<i>Informed player {self.id} that player {player_id} does not possess any {rank.name.upper()}s. {current_intel}</i><br>\n")
