from enum import Enum

class CardSuit(Enum):
    CLUBS = 0
    DIAMONDS = 1
    HEARTS = 2
    SPADES = 3

class CardRank(Enum):
    ACE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    SIX = 6
    SEVEN = 7
    EIGHT = 8
    NINE = 9
    TEN = 10
    JACK = 11
    QUEEN = 12
    KING = 13

def is_same_card(c1, c2):
    return c1.suit == c2.suit and c1.rank == c2.rank

class Card:

    min = 0
    max = (len(CardSuit) * len(CardRank)) - 1

    def __init__(self, suit, rank):
        assert isinstance(suit, CardSuit)
        assert isinstance(rank, CardRank)
        self.suit = suit
        self.rank = rank

    @classmethod
    def ordinal(cls, ordinal_no):
        assert ordinal_no >= Card.min and ordinal_no <= Card.max
        suit = CardSuit(int(ordinal_no / len(CardRank)))
        rank = CardRank((ordinal_no % len(CardRank))+1)
        return cls(suit, rank)

    @classmethod
    def deserialize(cls, s):
        parts = s.split(' of ')
        suit = CardSuit[parts[1].upper()]
        rank = CardRank[parts[0].upper()]
        return cls(suit, rank)

    def text_str(self):
        return str(self.rank.name.capitalize()) + ' of ' + str(self.suit.name.capitalize())
    
    def __str__(self):
        return self.text_str()

    def serialize(self):
        return Card.__str__(self)
