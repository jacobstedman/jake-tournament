from playingcards.basic.pile import Pile

class Deck(Pile):

    def __init__(self, card_class):
        super().__init__(card_class)
        self.list_of_cards = [card_class.ordinal(i) for i in range(card_class.min, card_class.max+1)]
