import unittest

from playingcards.basic.card import Card, CardSuit, CardRank
from playingcards.basic.hand import Hand

class HandTestCase(unittest.TestCase):

    def test_add(self):
        h = Hand()
        h.add(Card(CardSuit.CLUBS, CardRank.FIVE))
        self.assertEqual(str(h), '1 position: Five of Clubs')
        h.add(Card(CardSuit.DIAMONDS, CardRank.FIVE))
        self.assertEqual(str(h), '1 position: Five of Clubs and Diamonds')
        h.add(Card(CardSuit.DIAMONDS, CardRank.SIX))
        self.assertEqual(str(h), '2 positions: Five of Clubs and Diamonds, Six of Diamonds')
        h.add(Card(CardSuit.SPADES, CardRank.KING))
        self.assertEqual(str(h), '3 positions: Five of Clubs and Diamonds, Six of Diamonds, King of Spades')

    def test_remove(self):
        h = Hand()
        h.add(Card(CardSuit.CLUBS, CardRank.FIVE))
        h.add(Card(CardSuit.DIAMONDS, CardRank.FIVE))
        h.add(Card(CardSuit.DIAMONDS, CardRank.SIX))
        h.add(Card(CardSuit.SPADES, CardRank.KING))
        h.remove(CardRank.FIVE)
        self.assertEqual(str(h), '2 positions: Six of Diamonds, King of Spades')
        h.remove(CardRank.SIX)
        self.assertEqual(str(h), '1 position: King of Spades')
        h.remove(CardRank.KING)
        self.assertEqual(str(h), 'Empty hand')

    def test_in(self):
        h = Hand()
        h.add(Card(CardSuit.CLUBS, CardRank.SEVEN))
        self.assertTrue(CardRank.SEVEN in h)
        self.assertFalse(CardRank.EIGHT in h)
