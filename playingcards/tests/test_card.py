import unittest

from playingcards.basic.card import Card, CardSuit, CardRank

class CardTestCase(unittest.TestCase):

    def test_init(self):
        a = Card(CardSuit.CLUBS, CardRank.ACE)
        self.assertEqual(a.suit, CardSuit.CLUBS)
        self.assertEqual(a.rank, CardRank.ACE)

    def test_ordinal(self):
        self.assertEqual(Card.min, 0)
        self.assertEqual(Card.max, 51)
        a = Card.ordinal(0)
        self.assertEqual(a.suit, CardSuit.CLUBS)
        self.assertEqual(a.rank, CardRank.ACE)
        a = Card.ordinal(51)
        self.assertEqual(a.suit, CardSuit.SPADES)
        self.assertEqual(a.rank, CardRank.KING)
