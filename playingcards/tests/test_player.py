import unittest

from playingcards.basic.card import Card, CardSuit, CardRank
from playingcards.basic.player import Player

class PlayerTestCase(unittest.TestCase):

    def test_accept_reveal_and_remove(self):
        p = Player(0, None, None)

        p.accept_card(Card(CardSuit.CLUBS, CardRank.SIX))
        p.accept_card(Card(CardSuit.HEARTS, CardRank.SIX))
        p.accept_card(Card(CardSuit.SPADES, CardRank.JACK))
        self.assertEqual(p.reveal_rank(CardRank.SIX)[0].suit, CardSuit.CLUBS)
        self.assertEqual(p.reveal_rank(CardRank.SIX)[1].suit, CardSuit.HEARTS)
        self.assertEqual(p.reveal_rank(CardRank.JACK)[0].suit, CardSuit.SPADES)

        p.remove_from_hand(CardRank.SIX)
        self.assertEqual(p.reveal_rank(CardRank.SIX), [])
        self.assertEqual(p.reveal_rank(CardRank.JACK)[0].suit, CardSuit.SPADES)
