import unittest

from playingcards.basic.card import CardSuit, CardRank
from playingcards.html.pretty_card import PrettyCard

class PrettyCardTestCase(unittest.TestCase):

    def test_init(self):
        c = PrettyCard(CardSuit.CLUBS, CardRank.TWO)
        self.assertEqual(str(c), '<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Playing_card_club_2.svg/60px-Playing_card_club_2.svg.png">\n')
