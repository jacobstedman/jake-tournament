import unittest

from playingcards.basic.card import Card
from playingcards.basic.deck import Deck

class DeckTestCase(unittest.TestCase):

    def test_init(self):
        d = Deck(Card)
        self.assertEqual(len(d), 52)

    def test_pop(self):
        d = Deck(Card)
        temp = d.pop_first()
        self.assertEqual(len(d), 51)
