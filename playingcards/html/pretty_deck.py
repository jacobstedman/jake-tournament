from playingcards.basic.deck import Deck

class PrettyDeck(Deck):

    def __str__(self):
        return '\n'.join([str(c) for c in self.list_of_cards])
