from playingcards.basic.player import Player
from playingcards.html.pretty_hand import PrettyHand

class PrettyPlayer(Player):

    def __init__(self, player_id, brain, output):
        super().__init__(player_id, brain, output)
        self.hand = PrettyHand()
