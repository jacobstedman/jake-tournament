from playingcards.basic.hand import Hand

class PrettyHand(Hand):

    def __str__(self):
        if len(self.positions) == 0:
            return "<b>Empty hand</b>"
        else:
            s = ''
            for p in self.positions:
                for c in p.cards:
                    s = s + str(c)
            return s
