from playingcards.basic.card import Card, CardSuit, CardRank

class PrettyCard(Card):

    PREFIX = 'https://upload.wikimedia.org/wikipedia/commons/thumb/'

    URL = {
        CardSuit.CLUBS: {
            CardRank.ACE: '3/36/Playing_card_club_A.svg/60px-Playing_card_club_A.svg.png',
            CardRank.TWO: 'f/f5/Playing_card_club_2.svg/60px-Playing_card_club_2.svg.png',
            CardRank.THREE: '6/6b/Playing_card_club_3.svg/60px-Playing_card_club_3.svg.png',
            CardRank.FOUR: '3/3d/Playing_card_club_4.svg/60px-Playing_card_club_4.svg.png',
            CardRank.FIVE: '5/50/Playing_card_club_5.svg/60px-Playing_card_club_5.svg.png',
            CardRank.SIX: 'a/a0/Playing_card_club_6.svg/60px-Playing_card_club_6.svg.png',
            CardRank.SEVEN: '4/4b/Playing_card_club_7.svg/60px-Playing_card_club_7.svg.png',
            CardRank.EIGHT: 'e/eb/Playing_card_club_8.svg/60px-Playing_card_club_8.svg.png',
            CardRank.NINE: '2/27/Playing_card_club_9.svg/60px-Playing_card_club_9.svg.png',
            CardRank.TEN: '3/3e/Playing_card_club_10.svg/60px-Playing_card_club_10.svg.png',
            CardRank.JACK: 'b/b7/Playing_card_club_J.svg/60px-Playing_card_club_J.svg.png',
            CardRank.QUEEN: 'f/f2/Playing_card_club_Q.svg/60px-Playing_card_club_Q.svg.png',
            CardRank.KING: '2/22/Playing_card_club_K.svg/60px-Playing_card_club_K.svg.png',
        },
        CardSuit.DIAMONDS: {
            CardRank.ACE: 'd/d3/Playing_card_diamond_A.svg/60px-Playing_card_diamond_A.svg.png',
            CardRank.TWO: '5/59/Playing_card_diamond_2.svg/60px-Playing_card_diamond_2.svg.png',
            CardRank.THREE: '8/82/Playing_card_diamond_3.svg/60px-Playing_card_diamond_3.svg.png',
            CardRank.FOUR: '2/20/Playing_card_diamond_4.svg/60px-Playing_card_diamond_4.svg.png',
            CardRank.FIVE: 'f/fd/Playing_card_diamond_5.svg/60px-Playing_card_diamond_5.svg.png',
            CardRank.SIX: '8/80/Playing_card_diamond_6.svg/60px-Playing_card_diamond_6.svg.png',
            CardRank.SEVEN: 'e/e6/Playing_card_diamond_7.svg/60px-Playing_card_diamond_7.svg.png',
            CardRank.EIGHT: '7/78/Playing_card_diamond_8.svg/60px-Playing_card_diamond_8.svg.png',
            CardRank.NINE: '9/9e/Playing_card_diamond_9.svg/60px-Playing_card_diamond_9.svg.png',
            CardRank.TEN: '3/34/Playing_card_diamond_10.svg/60px-Playing_card_diamond_10.svg.png',
            CardRank.JACK: 'a/af/Playing_card_diamond_J.svg/60px-Playing_card_diamond_J.svg.png',
            CardRank.QUEEN: '0/0b/Playing_card_diamond_Q.svg/60px-Playing_card_diamond_Q.svg.png',
            CardRank.KING: '7/78/Playing_card_diamond_K.svg/60px-Playing_card_diamond_K.svg.png',
        },
        CardSuit.HEARTS: {
            CardRank.ACE: '5/57/Playing_card_heart_A.svg/60px-Playing_card_heart_A.svg.png',
            CardRank.TWO: 'd/d5/Playing_card_heart_2.svg/60px-Playing_card_heart_2.svg.png',
            CardRank.THREE: 'b/b6/Playing_card_heart_3.svg/60px-Playing_card_heart_3.svg.png',
            CardRank.FOUR: 'a/a2/Playing_card_heart_4.svg/60px-Playing_card_heart_4.svg.png',
            CardRank.FIVE: '5/52/Playing_card_heart_5.svg/60px-Playing_card_heart_5.svg.png',
            CardRank.SIX: 'c/cd/Playing_card_heart_6.svg/60px-Playing_card_heart_6.svg.png',
            CardRank.SEVEN: '9/94/Playing_card_heart_7.svg/60px-Playing_card_heart_7.svg.png',
            CardRank.EIGHT: '5/50/Playing_card_heart_8.svg/60px-Playing_card_heart_8.svg.png',
            CardRank.NINE: '5/50/Playing_card_heart_9.svg/60px-Playing_card_heart_9.svg.png',
            CardRank.TEN: '9/98/Playing_card_heart_10.svg/60px-Playing_card_heart_10.svg.png',
            CardRank.JACK: '4/46/Playing_card_heart_J.svg/60px-Playing_card_heart_J.svg.png',
            CardRank.QUEEN: '7/72/Playing_card_heart_Q.svg/60px-Playing_card_heart_Q.svg.png',
            CardRank.KING: 'd/dc/Playing_card_heart_K.svg/60px-Playing_card_heart_K.svg.png',
        },
        CardSuit.SPADES: {
            CardRank.ACE: '2/25/Playing_card_spade_A.svg/60px-Playing_card_spade_A.svg.png',
            CardRank.TWO: 'f/f2/Playing_card_spade_2.svg/60px-Playing_card_spade_2.svg.png',
            CardRank.THREE: '5/52/Playing_card_spade_3.svg/60px-Playing_card_spade_3.svg.png',
            CardRank.FOUR: '2/2c/Playing_card_spade_4.svg/60px-Playing_card_spade_4.svg.png',
            CardRank.FIVE: '9/94/Playing_card_spade_5.svg/60px-Playing_card_spade_5.svg.png',
            CardRank.SIX: 'd/d2/Playing_card_spade_6.svg/60px-Playing_card_spade_6.svg.png',
            CardRank.SEVEN: '6/66/Playing_card_spade_7.svg/60px-Playing_card_spade_7.svg.png',
            CardRank.EIGHT: '2/21/Playing_card_spade_8.svg/60px-Playing_card_spade_8.svg.png',
            CardRank.NINE: 'e/e0/Playing_card_spade_9.svg/60px-Playing_card_spade_9.svg.png',
            CardRank.TEN: '8/87/Playing_card_spade_10.svg/60px-Playing_card_spade_10.svg.png',
            CardRank.JACK: 'b/bd/Playing_card_spade_J.svg/60px-Playing_card_spade_J.svg.png',
            CardRank.QUEEN: '5/51/Playing_card_spade_Q.svg/60px-Playing_card_spade_Q.svg.png',
            CardRank.KING: '9/9f/Playing_card_spade_K.svg/60px-Playing_card_spade_K.svg.png',
        },
    }

    def __str__(self):
        return '<img src="' + PrettyCard.PREFIX + PrettyCard.URL[self.suit][self.rank] + '">\n'
